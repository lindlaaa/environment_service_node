var express = require('express')
var morgan = require('morgan')
mongoose = require('mongoose')
Task = require('./api/models/sensorDataModel')
bodyParser = require('body-parser')
fs = require('fs')
path = require('path')

app = express()
port = process.env.PORT || 3000

// log only 4xx and 5xx responses to console
app.use(morgan('dev', {
    skip: function (req, res) { return res.statusCode < 400 }
}))

// log all requests to access.log
app.use(morgan('common', {
    stream: fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'})
}))

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/SensorDB')
    .catch(err => { // mongoose connection error will be handled here
        console.error('App starting error:', err.stack);
        process.exit(1);
    })


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/sensorRoutes');
routes(app); //register the route


app.listen(port);
console.log('Sensor RESTful API server started on: ' + port);
