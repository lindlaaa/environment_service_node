'use strict';


var mongoose = require('mongoose'),
Data = mongoose.model('sensorData');

exports.test = function(req, res) {
  res.send('Hello world!')
}

exports.list_all_data = (req, res) => {
  Data.find({}, function(err, data){
      res.send(data);
  });
}


exports.create_a_data = function(req, res) {
  var new_data = new Data(req.body);
  new_data.save(function(err, data) {
    if (err)
      res.send(err);
    res.json(data);
  });
};


exports.read_a_data = function(req, res) {
  Data.find({Created_date: req.body.date}, function(err, data) {
    if (err)
      res.send(err);
    res.json(data);
  });
};
