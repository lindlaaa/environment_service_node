'use strict';
module.exports = function(app) {
  var sensorController = require('../controllers/sensorController');

  app.route('/test')
    .get(sensorController.test)

  app.route('/data')
    .get(sensorController.list_all_data)
    .post(sensorController.create_a_data);


  app.route('/data/recent/:date')
    .get(sensorController.read_a_data)
};
