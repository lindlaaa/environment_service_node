'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var sensorDataSchema = new Schema({
  temperature: {
    type: Schema.Types.Decimal128,
    required: 'Kindly enter the name of the task'
  },
  pressure: {
    type: Schema.Types.Decimal128,
    required: 'Enter the pressure'
  },
  humidity: {
    type: Schema.Types.Decimal128,
    required: 'Enter the humidity'
  },
  Created_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('sensorData', sensorDataSchema);
